//package th.ac.kku.mactanomsup.smarthome4;
//
//import android.content.Context;
//import android.os.Handler;
//import android.widget.Toast;
//
//import com.github.nkzawa.emitter.Emitter;
//import com.github.nkzawa.socketio.client.IO;
//import com.github.nkzawa.socketio.client.Socket;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.net.URISyntaxException;
//
///**
// * Created by mactanomsup on 5/3/16.
// */
//public class SocketIO {
//
//    String[] temperature_data;
//    String ip = "";
//
//    private Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket(ip);
//        } catch (URISyntaxException e) {}
//    }
//
//    public void connect(){
//        mSocket.disconnect();
//
//        temperature_data = new String[2];
//        temperature_data[0] = "0";
//        temperature_data[1] = "0";
//
//        final RoomActivity roomActivity = new RoomActivity();
//
//        roomActivity.showToast("กำลังเข้าใช้งาน");
//        ip = "http://" + MainActivity.ip_tv.getText().toString();
//
//        try {
//            mSocket = IO.socket(ip);
//
//        } catch (URISyntaxException e) {
//            roomActivity.showToast("ไม่สามารถเข้าใช้งานได้");
//        }
//
//        mSocket.connect();
//        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
//        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//        mSocket.emit("message", "มือถือได้รับการเชื่อมต่อเข้ามาแล้ว");
//        mSocket.on("LVR_Temp", temperature);
//
//        Handler myHandler = new Handler();
//        myHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mSocket.connected() == true) {
//                    roomActivity.showToast("เข้าใช้งานเรียบร้อยแล้ว");
//                    //Intent intent = new Intent(RoomActivity.this, RoomActivity.class);
//                    //startActivity(intent);
//                } else {
//                    roomActivity.showToast("ไม่สามารถเข้าใช้งานได้");
//                }
//
//
//            }
//        }, 2000);
//    }
//
//
//    private Emitter.Listener onConnectError = new Emitter.Listener() {
//
//        @Override
//        public void call(Object... args) {
//
//
//        }
//    };
//
//    private Emitter.Listener temperature = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    JSONObject data = (JSONObject) args[0];
//                    String temp;
//                    String humi;
//                    try {
//                        temp = data.getString("temp");
//                        humi = data.getString("humidity");
//                        temperature_data[0] = temp;
//                        temperature_data[1] = humi;
//                        // temp_tv.setText(temperature_data[0] + "℃");
//                    } catch (JSONException e) {
//                        return;
//                    }
//                }
//            });
//        }
//    };
//
//    public String[] getTemperature() {
//        return temperature_data;
//    }
//
//    public void sendRelay(int number, int state) {
//
//        JSONObject data  = new JSONObject();
//
//        try{
//            data.put("number", number);
//            data.put("state", state);
//        }catch (Exception e){
//
//        }
//
//        mSocket.emit("LVR_Relay", data);
//    }
//
//
//
//
//
//
//}

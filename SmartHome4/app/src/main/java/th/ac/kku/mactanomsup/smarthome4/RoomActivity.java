package th.ac.kku.mactanomsup.smarthome4;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class RoomActivity extends AppCompatActivity {

    static String[] temperature_data;
    String ip = "";
    public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        temperature_data = new String[2];
        temperature_data[0] = "0";
        temperature_data[1] = "0";

        connect();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Living Room"));
        tabLayout.addTab(tabLayout.newTab().setText("Kitchen Room"));
        tabLayout.addTab(tabLayout.newTab().setText("Bath Room"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        th.ac.kku.mactanomsup.smarthome4.PagerAdapter  adapter = new th.ac.kku.mactanomsup.smarthome4.PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mSocket.on("LVR_Temp", temperature);
    }


    public void speakNow() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speech recognition demo");
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            // Fill the list view with the strings the recognizer thought it
            // could have heard
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            //mList.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, matches));
            // matches is the result of voice input. It is a list of what the
            // user possibly said.
            // Using an if statement for the keyword you want to use allows the
            // use of any activity if keywords match
            // it is possible to set up multiple keywords to use the same
            // activity so more than one word will allow the user
            // to use the activity (makes it so the user doesn't have to
            // memorize words from a list)
            // to use an activity from the voice input information simply use
            // the following format;
            // if (matches.contains("keyword here") { startActivity(new
            // Intent("name.of.manifest.ACTIVITY")

            if (matches.contains("turn on all switches")) {
                Living_tab.tgb_lv_bulb1.setChecked(true);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Living_tab.tgb_lv_bulb2.setChecked(true);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Living_tab.tgb_lv_bulb3.setChecked(true);
            }

            if (matches.contains("turn off all switches")) {
                Living_tab.tgb_lv_bulb1.setChecked(false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Living_tab.tgb_lv_bulb2.setChecked(false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Living_tab.tgb_lv_bulb3.setChecked(false);
            }

            if (matches.contains("switch one on") ) {
                //sendRelay(1,1);
                Living_tab.tgb_lv_bulb1.setChecked(true);
            }
            if (matches.contains("switch two on") ) {
                Living_tab.tgb_lv_bulb2.setChecked(true);
                //sendRelay(2,1);
            }
            if (matches.contains("switch three on")) {
                Living_tab.tgb_lv_bulb3.setChecked(true);
                //sendRelay(3,1);
            }
            if (matches.contains("switch one off")) {
                Living_tab.tgb_lv_bulb1.setChecked(false);
                //sendRelay(1,0);
            }
            if (matches.contains("switch two off") ) {
                Living_tab.tgb_lv_bulb2.setChecked(false);
                //sendRelay(2,0);
            }
            if (matches.contains("switch three off") ) {
                Living_tab.tgb_lv_bulb3.setChecked(false);
                //sendRelay(3,0);
            }
        }
    }

    public void countdownTime(final String time) {
        int i = Integer.parseInt(time);

        MediaPlayer mp = MediaPlayer.create(RoomActivity.this, R.raw.switchison);
        mp.start();

        sendRelay(4,1);

        Living_tab.settime_bt.setVisibility(View.GONE);
        Living_tab.title_tv.setVisibility(View.VISIBLE);
        Living_tab.time_tv.setVisibility(View.VISIBLE);
        Living_tab.layout.setVisibility(View.VISIBLE);

        new CountDownTimer(i*60000, 1000) {

            public void onTick(long millisUntilFinished) {
                Living_tab.milliseconds = millisUntilFinished;
                Living_tab.seconds = (int) ( Living_tab.milliseconds / 1000) % 60 ;
                Living_tab.minutes = (int) (( Living_tab.milliseconds / (1000*60)) % 60);
                Living_tab.time_tv.setText(String.valueOf( Living_tab.minutes) + " minute(s) " + String.valueOf( Living_tab.seconds) + " second(s)");
                Living_tab.cancel_bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onFinish();
                    }
                });
            }

            @Override
            public void onFinish() {

                MediaPlayer mp = MediaPlayer.create(RoomActivity.this, R.raw.switchisoff);
                mp.start();

                sendRelay(4,0);
                Living_tab.settime_bt.setVisibility(View.VISIBLE);
                Living_tab.title_tv.setVisibility(View.GONE);
                Living_tab.time_tv.setVisibility(View.GONE);
                Living_tab.layout.setVisibility(View.GONE);
                showToast("Relay 4 is off");
                cancel();
                Living_tab.seconds = 0;
            }

        }.start();
    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(ip);
        } catch (URISyntaxException e) {}
    }

    public void connect(){
        mSocket.disconnect();
        showToast("Login");
        ip = "http://" + MainActivity.ip_tv.getText().toString();

        try {
            mSocket = IO.socket(ip);

        } catch (URISyntaxException e) {
            showToast("Cannot reach to the server");
        }

        mSocket.connect();
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.emit("message", "มือถือได้รับการเชื่อมต่อเข้ามาแล้ว\n" + "ผู้ใช้ชื่อ : " + MainActivity.user_tv.getText().toString());
        mSocket.on("LVR_Temp", temperature);
        mSocket.on("LVR_PIR", pir);

        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSocket.connected() == true) {
                    showToast("Connected to Raspberry Pi server");
                    MediaPlayer mp = MediaPlayer.create(RoomActivity.this, R.raw.homecontrol);
                    mp.start();
                } else {
                    showToast("Cannot reach to the server");
//                    Intent intent = new Intent(RoomActivity.this, MainActivity.class);
//                    startActivity(intent);
                }


            }
        }, 2000);
    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {

        @Override
        public void call(Object... args) {


        }
    };

    private Emitter.Listener temperature = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String temp;
                    String humi;
                    try {
                        temp = data.getString("temp");
                        humi = data.getString("humidity");
                        temperature_data[0] = temp;
                        temperature_data[1] = humi;
                        Living_tab.temp_tv.setText(temperature_data[0] + " ℃");
                        Living_tab.arcView.addEvent(new DecoEvent.Builder(Float.parseFloat(temperature_data[1])).setIndex(Living_tab.series1Index).setDelay(12000).build());
                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };
    int i = 15;
    private Emitter.Listener pir = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    int intrude;
                    try {
                        intrude = data.getInt("pir");
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        if(intrude == 1) {
                            try {
                                if(i>0) {
                                    r.play();
                                    i--;
                                } else {
                                    r.stop();
                                    i = 15;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                                r.stop();
                        }
                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };

    public void sendRelay(int number, int state) {

        JSONObject data  = new JSONObject();

        try{
            data.put("number", number);
            data.put("state", state);
        }catch (Exception e){

        }

        mSocket.emit("LVR_Relay", data);
    }

    public void showToast(String str) {
        Context context = getApplicationContext();
        CharSequence text = str;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}

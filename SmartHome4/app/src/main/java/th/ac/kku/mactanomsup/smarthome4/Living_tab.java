package th.ac.kku.mactanomsup.smarthome4;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.EdgeDetail;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.charts.SeriesLabel;
import com.hookedonplay.decoviewlib.events.DecoEvent;

public class Living_tab extends Fragment implements NumberPicker.OnValueChangeListener, View.OnClickListener{

    View lv_view;
    static DecoView arcView;
    String format = "%.0f%%";
    static TextView temp_tv, time_tv, title_tv;
    static SeriesItem seriesItem1;
    static int series1Index;
    static ToggleButton tgb_lv_bulb1, tgb_lv_bulb2, tgb_lv_bulb3;
    static ImageButton settime_bt;
    static Button cancel_bt,mic_bt;
    static LinearLayout layout;
    static long milliseconds;
    static int seconds,minutes;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        lv_view = inflater.inflate(R.layout.activity_living_tab, container, false);
        initInstance();

        if(savedInstanceState != null) {
            if(seconds != 0) {
                settime_bt.setVisibility(View.GONE);
                time_tv.setVisibility(View.VISIBLE);
                cancel_bt.setVisibility(View.VISIBLE);
                title_tv.setVisibility(View.VISIBLE);
                layout.setVisibility(View.VISIBLE);
                time_tv.setText(String.valueOf(minutes) + " minute(s) " + String.valueOf(seconds) + " second(s)");
            }
            arcView.addEvent(new DecoEvent.Builder(Float.parseFloat(RoomActivity.temperature_data[1])).setIndex(Living_tab.series1Index).setDelay(1000).build());
        }

        tgb_lv_bulb1.setOnCheckedChangeListener(bulb1_listener);
        tgb_lv_bulb2.setOnCheckedChangeListener(bulb2_listener);
        tgb_lv_bulb3.setOnCheckedChangeListener(bulb3_listener);

        mic_bt.setOnClickListener(this);
        settime_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });

        graph();
        return lv_view;
    }

    private void initInstance() {
        temp_tv = (TextView) lv_view.findViewById(R.id.temp_tv);
        arcView = (DecoView) lv_view.findViewById(R.id.dynamicArcView);
        tgb_lv_bulb1 = (ToggleButton) lv_view.findViewById(R.id.tgb_lv_bulb1);
        tgb_lv_bulb2 = (ToggleButton) lv_view.findViewById(R.id.tgb_lv_bulb2);
        tgb_lv_bulb3 = (ToggleButton) lv_view.findViewById(R.id.tgb_lv_bulb3);
        settime_bt= (ImageButton) lv_view.findViewById(R.id.settime);
        time_tv = (TextView) lv_view.findViewById(R.id.time_tv);
        title_tv = (TextView) lv_view.findViewById(R.id.title_tv);
        cancel_bt = (Button) lv_view.findViewById(R.id.cancel_bt);
        layout = (LinearLayout) lv_view.findViewById(R.id.cancel_layout);
        mic_bt = (Button) lv_view.findViewById(R.id.mic_bt);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save things here
        outState.putBoolean("relay1", tgb_lv_bulb1.isChecked());
        outState.putBoolean("relay2", tgb_lv_bulb2.isChecked());
        outState.putBoolean("relay3", tgb_lv_bulb3.isChecked());
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    public void show() {

        MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.pickanumber);
        mp.start();

        final Dialog d = new Dialog(getActivity());
        d.setTitle("Set timer");

        d.setContentView(R.layout.dialog);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);

        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(999);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);

        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                    showToast("Start counting down " + String.valueOf(np.getValue()) + " minute(s)");
                    ((RoomActivity) getActivity()).countdownTime(String.valueOf(np.getValue()));
                    d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void graph() {
        // Create background track
        arcView.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(20f)
                .build());

        //Create data series track
        seriesItem1 = new SeriesItem.Builder(Color.argb(255, 64, 196, 0))
                .setRange(0, 100, 0)
                .setLineWidth(50f)
                .addEdgeDetail(new EdgeDetail(EdgeDetail.EdgeType.EDGE_INNER, Color.parseColor("#22000000"), 0.4f))
                .setSeriesLabel(new SeriesLabel.Builder("Humidity %.0f%% ").build())
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(false)
                .setInset(new PointF(32f, 32f))
                .setDrawAsPoint(false)
                .setSpinClockwise(true)
                .setSpinDuration(6000)
                .build();

        series1Index = arcView.addSeries(seriesItem1);
        
        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        arcView.addEvent(new DecoEvent.Builder(50).setIndex(series1Index).setDelay(4000).build());
        arcView.addEvent(new DecoEvent.Builder(90).setIndex(series1Index).setDelay(8000).build());


        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                if (format.contains("%%")) {
                    float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                } else {

                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
    }

    CompoundButton.OnCheckedChangeListener bulb1_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.v("TOGGLE CHECK CHANGED 1",String.valueOf(isChecked));
            if (isChecked) {
                ((RoomActivity) getActivity()).sendRelay(1,1);
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchison);
                mp.start();
            } else {
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchisoff);
                mp.start();
                ((RoomActivity) getActivity()).sendRelay(1,0);
            }

        }
    };

    CompoundButton.OnCheckedChangeListener bulb2_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.v("TOGGLE CHECK CHANGED 2",String.valueOf(isChecked));
            if (isChecked) {
                ((RoomActivity) getActivity()).sendRelay(2,1);
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchison);
                mp.start();
            } else {
                ((RoomActivity) getActivity()).sendRelay(2,0);
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchisoff);
                mp.start();
            }

        }
    };

    CompoundButton.OnCheckedChangeListener bulb3_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.v("TOGGLE CHECK CHANGED 3",String.valueOf(isChecked));
            if (isChecked) {
                ((RoomActivity) getActivity()).sendRelay(3,1);
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchison);
                mp.start();
            } else {
                ((RoomActivity) getActivity()).sendRelay(3,0);
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.switchisoff);
                mp.start();
            }

        }
    };

    public void showToast(String str) {
        Context context = getContext();
        CharSequence text = str;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    @Override
    public void onClick(View v) {
        ((RoomActivity) getActivity()).speakNow();
    }
}

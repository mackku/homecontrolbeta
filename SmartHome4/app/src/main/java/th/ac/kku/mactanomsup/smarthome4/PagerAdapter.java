package th.ac.kku.mactanomsup.smarthome4;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by mactanomsup on 4/28/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Living_tab tab1 = new Living_tab();
                return tab1;
            case 1:
                Kitchen_tab tab2 = new Kitchen_tab();
                return tab2;
            case 2:
                Bathroom_tab tab3 = new Bathroom_tab();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
